def limits(n: int) -> tuple[int, int]:
    DIGITS = "123456789"
    size = len(str(n))
    return int(DIGITS[:size]), int(DIGITS[-size:])

def next_reading(reading: int):
    start, limit = limits(reading)
    size = len(str(reading))
    top = 9
    if reading == limit:
        return start 
    elif reading % 10 != top and len(str(reading)) == size:
        return reading + 1
    while reading % 10 == top:
        top -= 1
        reading //= 10
    reading += 1
    while len(str(reading)) != size:
        reading = (reading * 10) + (reading % 10 + 1)
    return reading



    
print(next_reading(5789))