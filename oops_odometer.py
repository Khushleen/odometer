DIGITS = '123456789'

def min_max(size: int) -> tuple[int, int]:
    return int(DIGITS[:size]), int(DIGITS[-size:])

class Odometer:
    def __init__(self, size: int) -> None:
        self._size = size
        self._min, self._max = min_max(size)
        self._reading = self._min

    def reading(self) -> int:
        return self._reading

    def __str__(self) -> str:
        return f'<{self._reading}>'
    
    def __repr__(self) -> str:
        size = len(str(self._reading))
        return f'Size: {size}, Reading: {self._reading}'
    
    def __eq__(self, other) -> bool:
        if self._size != other._size:
            raise ValueError(f"Incomparable Odometers: sizes {self._size}, {other._size}")
        return self._reading == other._reading
    
    def __lt__(self, other) -> bool:
        if self._size != other._size:
            raise ValueError("Odometers of different sizes are incomparable")
        return self._reading < other._reading
    
    @classmethod
    def is_ascending(cls, reading) -> bool:
        return all(a < b for a, b in zip(str(reading), str(reading)[1:]))

    def forward(self, step:int=1) -> None:
        copy = Odometer(self._size)
        copy._reading = self._reading
        for _ in range(step):
            if copy._reading == copy._max:
                copy._reading = copy._min
            else:
                copy._reading += 1
                while not Odometer.is_ascending(copy._reading):
                    copy._reading += 1
        return copy
    
    def backward(self, step:int=1) -> None:
        copy = Odometer(self._size)
        copy._reading = self._reading
        for _ in range(step):
            if copy._reading == copy._min:
                copy._reading = copy._max
            else:
                copy._reading -= 1
                while not Odometer.is_ascending(copy._reading):
                    copy._reading -= 1
        return copy

    def distance(self, other):
        if o1._size != other._size:
            raise ValueError("Odometers of different sizes are incomparable")
        count = 0
        copy = Odometer(self._size)
        copy._reading = self._reading
        while copy._reading != other._reading:
            copy = copy.forward()
            count += 1
        return count
    
    


o1 = Odometer(3)
o2 = Odometer(6)
#print(o1)
o1._reading = 589

o3 = Odometer(5)
o3._reading = 45789
print(o3.forward(5))

try:
    print(o1.distance(o2))
except ValueError as e:
    print(e)