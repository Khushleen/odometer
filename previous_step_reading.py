from previous_reading import previous_reading

def previous_step_reading(reading, step):
    for i in range(step):
        reading = previous_reading(reading)
    return reading

print(previous_step_reading(123, 1))