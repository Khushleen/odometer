from general_functions import is_ascending, limits

def previous_reading(reading: int) -> int:
    start, limit = limits(reading)

    if reading == start:
        return limit
    else:
        reading -= 1
        while not is_ascending(reading):
            reading -= 1
        return reading

    