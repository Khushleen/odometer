def is_ascending(reading: int) -> bool:
    return list(str(reading)) == sorted(set(str(reading)))

def limits(n: int) -> tuple[int, int]:
    DIGITS = "123456789"
    size = len(str(n))
    return int(DIGITS[:size]), int(DIGITS[-size:])