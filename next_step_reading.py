from next_reading import next_reading

def next_step_reading(reading, step):
    for i in range(step):
        reading = next_reading(reading)
    return reading

print(next_step_reading(123, 10))