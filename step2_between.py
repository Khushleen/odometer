from next_reading import next_reading

def steps_between(reading1, reading2) -> int:
    
    count = 0
    while reading2 != reading1:
        reading1 = next_reading(reading1)
        count += 1
    return count
