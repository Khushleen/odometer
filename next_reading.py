from general_functions import is_ascending, limits

def next_reading(reading: int) -> int:
    start, limit = limits(reading)

    if reading == limit:
        return start
    else:
        reading += 1
        while not is_ascending(reading):
            reading += 1
        return reading

