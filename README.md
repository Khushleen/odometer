#  ODOMETER
Given a 3-digit odometer with the following constraints

1. It can not have 0 as one of the digits
2. The reading can only be in strictly ascending order
3. The size of the odometer is fixed

Create the following functions:

1. given a reading, tell the next reading
2. given a reading,  previous reading
3. given reading & step count, give reading after going that many steps forward
4. given reading & step count, give reading after going that many steps backward
5. given two readings, calculate steps taken to reach the readngs

